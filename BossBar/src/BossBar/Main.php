<?php

namespace BossBar;

use pocketmine\event\Listener;
use pocketmine\plugin\PluginBase;
use pocketmine\Server;
use pocketmine\Player;
use pocketmine\event\player\PlayerJoinEvent;
use pocketmine\network\protocol\MoveEntityPacket;
use pocketmine\network\protocol\SetEntityDataPacket;
use pocketmine\event\entity\EntityLevelChangeEvent;
use pocketmine\entity\Entity;
use pocketmine\utils\UUID;
use pocketmine\scheduler\CallbackTask;

class Main extends PluginBase implements Listener{

	public $eid = [], $i = 0;

	public function onEnable(){
		$this->getServer()->getPluginManager()->registerEvents($this, $this);
		$this->getServer()->getNetwork()->registerPacket(BossEventPacket::NETWORK_ID, BossEventPacket::class);
		$this->getServer()->getNetwork()->registerPacket(UpdateAttributesPacket::NETWORK_ID, UpdateAttributesPacket::class);
		$this->getServer()->getNetwork()->registerPacket(SetEntityDataPacket::NETWORK_ID, SetEntityDataPacket::class);
	}
	
	public function onJoin(PlayerJoinEvent $ev){
		 $this->spawnBossBar($ev->getPlayer());
	}
	
    public function onTp(EntityLevelChangeEvent $event){
         if ($event->getEntity() instanceof Player){
         	 $player = $event->getEntity();
         	 $fakeboss = new FakeWither();
         	 //$fakeboss->despawnFrom($player);
$this->getServer()->getScheduler()->scheduleDelayedTask(new CallbackTask([$this,"spawnBossBar" ], [$player]), 40);
         }
    }

	public function spawnBossBar(Player $player){
		$fakeboss = new FakeWither();
		$fakeboss->init();
		$fakeboss->spawnTo($player);

	}
	
	/*  
	 * @progress number is between 0 and 100 
	 * @text text higher health bar
	 */
	 
	public function sendBossBar($player, $progress, $text){
		if(count($this->getServer()->getOnlinePlayers()) > 0) $this->i < 100?$this->i++:$this->i = 0;
		else return;
		$eid = 1000; /* $this->eid[$player->getName()]; */ // TODO: fix

		$mpk = new MoveEntityPacket;
	    $mpk->eid = 1000;
    	$mpk->x = $player->x;
	    $mpk->y = $player->y + 25;
	    $mpk->z = $player->z;
	    // $mpk->yaw = $player->yaw;
	    // $mpk->headYaw;
	    // $mpk->pitch;
	    $this->getServer()->broadcastPacket([$player], $mpk);
		
		$upk = new UpdateAttributesPacket(); // Change health of fake wither -> bar progress
		$upk->entries[] = new BossBarValues(0, 300, max(1, min([$progress, 100])) / 100 * 300, 'minecraft:health'); // Ensures that the number is between 0 and 100;
		$upk->entityId = $eid;
		//$this->getServer()->broadcastPacket($this->getServer()->getOnlinePlayers(), $upk);
		$this->getServer()->broadcastPacket([$player], $upk);
		
		$npk = new SetEntityDataPacket(); // change name of fake wither -> bar text
		$npk->metadata = [Entity::DATA_NAMETAG => [Entity::DATA_TYPE_STRING,  $text]];
		$npk->eid = $eid;
		//$this->getServer()->broadcastPacket($this->getServer()->getOnlinePlayers(), $npk);
		$this->getServer()->broadcastPacket([$player], $npk);
		
		$state = 0;
		$bpk = new BossEventPacket(); // TODO: check if can be removed
		$bpk->eid = $eid;
		$bpk->state = 0;
		//$this->getServer()->broadcastPacket($this->getServer()->getOnlinePlayers(), $bpk);
		$this->getServer()->broadcastPacket([$player], $bpk);
	}
}